[[_TOC_]]

# Nehemia Katalog

Erstellt einen einfachen Produkte-Katalog von EShop Produkten.

# Anleitung und Informationen

Es handelt sich um ein Frontend-Modul, welches einen einfachen Katalog erstellt. Auf edition-nehemia.ch ist das Modul nur als Administrator zugänglich. Man kann Kataloge nach Kategorie oder Sichtwort erstellen.

eBooks werden generell nicht gelistet, sowie Produkte, deren Lagerbestand unter 20 Ex. (dieser Wert kann im Backend verändert werden) ist oder die nicht veröffentlicht sind.

Der Titel des Kataloges kann über zwei Zeilen gehen.

Aktionspreise werden nur berücksichtigt, wenn sie unter "Spezial" eingetragen wurden.

(Wurde schon ein Katalog exportiert und werden dann nur einige Korrekturen durchgeführt (im Joomla Backend) müssen im Cache die Bilddaten gelöscht werden, damit eine korrigierte Ausgabe erzeugt wird.)

## Kategorien
Es können eine oder mehrere Kategorien zusammen erstellt werden. Zur Eingabe der Kategorie müssen die verschiedene ID getrennt von einem Komma eingegeben werden (z.B. 5,6,7,8). Die Eingabe bestimmt auch die Reihenfolge.

Wenn man des Schlüsselwort `Alle {Kategorien}` (mit Leerschlag vor der Kategorie; z. B., `Alle 2,3`) eingibt werden alle Produkte der Kategorie, inkl. diesen Produkten, die eine andere Hauptkategorie haben, ausgegeben. 

## Stichworte
Werden Stichworte als Filter verwendet, wird nur das exakte Wort, bzw. Begriff gefunden. Dies im Titel oder in der Beschreibung des Produktes. Oder auch in den Produkttags.
Stichwort und Kategorien werden über dasselbe Textfeld übermittelt.   
Mit einem `*` Zeichen am Ende des Stichworts wird die genaue Suche aufgehoben und das Wortende eines Treffers ist offen.   

## Neue Produkte
Es können auch Kataloge von neuen Produkten erstellt werden. Dazu gibt `Neu {AnzahlProdukte} {Stichwort}` ein. Auch `Neu {AnzahlProdukte} Alle {Kategorien}` funktioniert, die neusten Bücher ein spezielen Kategorie zu bekommen.

# Optionen
Soweit das Layout es unterstützt, können diese Optionen gemacht werden.

## Im Backend in den Moduleinstellungen
Folgende Dinge können im Backend eingestellt werden.
* Ab welchem Lagerbestand ein Produkt in den Katalog aufgenommen werden kann.
* Wer der Rechtsinhaber ist (auf Umschlagseite 3).
* Ein Hinweis zu den Katalogrechten (auf Umschlagseite 3).

## Katalog:keinRahmen:
Wird im "Tab 5 Content" des Produktes der Text "Katalog:keinRahmen" eingegeben, wird im Katalog kein Rahmen ausgegeben. Achtung: Das Feld "Tab 5 Title" muss unbedingt leer sein, ansonsten wir der Text im Frontend ausgegeben.

## Katalog:nielisten
Wird im "Tab 5 Content" des Produktes der Text "Katalog:nielisten" eingegeben, wird dieses Produkt im Katalog grundsätzlich nie gelistet.

# Offene Punkte

Gewisse Unicode-Zeichen werden nicht unterstützt. Es gibt jedoch eine Möglichkeit die Folgen abzufedern. Nämlich die Zeichen, welche nicht funktionierten in der Funktion "ersetzeUnicodezeichen" manuell korrigieren.

# Layouts

## Detail-Layout
Gibt alle Informationen aus. Beschreibungen werden ab 550 Zeichen gekürzt.

## Nehemia-Layout
Das Standartlayout für Kataloge von Edition Nehemia.

### Besonderheiten
Der Untertitel wird aus der Beschreibung herausgelesen. Er ist, wenn vorhanden immer am Anfang und ist kursiv und bold gesetzt.

## Tabitha-Layout
Das Standartlayout für Kleiderkataloge von Tabitha.

## Übersicht-Layout
Gibt nur die wichtigsten Dinge aus.

## Anschliessende PDF Bearbeitung
Um die volle Katalogzahl zu erreichen, muss oft manuell noch Seiten eingefügt werden. Die funktioniert nicht in allen Programmen. Empfohlen wird Pdf24 (https://www.pdf24.org/de/). Beim Speichern muss jedoch beachtet werden, dass man das PDF nicht mit einer neuen Qualität speichert, sondern die Option "Nur zusammenstellen" wählt.

# Layout hinzufügen
Eine kurze Anleitung, wie man neue Layouts hinzufügt.

## Datenstruktur
* Alle CSS Dateien werden im Ordner "css" abgespeichert und tragen den Namen {LAYOUTNAME}_css.php. Die CSS Datei wird als PHP String definiert.    
* Alle Produktblöcke werden im Ordner "produktebloecke" abgespeichert und tragen den Namen {LAYOUTNAME}.php 

# Funktionen der Tools-Klasse
Ein Überblick

## kuerzenText(string $Text, int $Zeichen = 550)
Kürzt den Text auf eine bestimmte Länge. Abgebrochen wird der Text beim letzten Zeilenumbruch, bzw. Absatzende. Dabei werden nur die Zeichen inkl. Leerzeichen beachtet!

## Attribute(int $ProduktID,string $Format = '',array $AttributeAuschliessen = array())
Gibt die Attribute eines Produktes zurück. Mit Format kann man die Standartausrichtung von vertikal überschreiben. Ist das Format nicht vertikal, werden die Angaben horizontal, d.h. nacheinander dargestellt. Diese Funktion ist nötig, da DomPDF die CSS-Funktionen display: flex; und :after nicht unterstützt.

## Preis(string $Preis,int $ID)
In dieser Funktion wird der Preis, der Aktionspreis oder die Preisreduzierung zu zurückgegeben.

To do: AlleFunktionen beschreiben

# Anleitung neues Layout
1. Ein bestehendes Layout kopieren (css & produktbloecke) und nach obengenannten Kriterien umbenennen. Achtung die Rechtschreibung inkl. Gross- / Kleinschreibung muss stimmen. 
2. Layout anpassen.   
3. Neue Umschlagseiten im Standartbilderordner von Joomla im Ordner "nehemia-katalog" abspeichern.    
3.1. Schreibweise {LAYOUTNAME}_umschlag{1-4}.jpg   
4. Im Frontend die Seite neu laden und das Layout sollte sichtbar sein.
## Neues HTML Layout
Wie oben nur beim Namen "_html" anfügen.

# Spezialcode
Beim Katalog «Bücher & und Vorträge» «von Rudolf Ebertshäuser» mit Stichwort «Rudolf Ebertshäuser» folgenden Code einfügen in Query für Produktabfrage
```
            // RE Katalog    
            ->where('b.product_desc NOT LIKE "%ESRA-Schriftendienst%"')      
```


Und das Array sortieren nach der Zeile «$this->ProduktObjectArray = $db->loadObjectList();»
```
        // Katalog RE    
        function SortierungTreffer($a, $b)    
        {    
            $A = 1;    
            if(preg_match('/Vortrag/',htmlentities($a->product_desc) . htmlentities($a->product_name)) == true     
            or preg_match('/MP3/',htmlentities($a->product_desc) . htmlentities($a->product_name)) == true) {     
                $A = $A -1;    
            }    
            $B = 1;    
            if(preg_match('/Vortrag/',htmlentities($b->product_desc)  . htmlentities($b->product_name)) == true     
            or preg_match('/MP3/',htmlentities($b->product_desc) . htmlentities($b->product_name)) == true) {    
                $B = $B -1;    
            }     
            
            if ($A == $B) {     
                $A= intval($a->id);     
                $B= intval($b->id);    
            }     
            
            if ($A < $B) {    
                return +1;    
            } else {    
                return -1;    
            }    
        }    
        usort($this->ProduktObjectArray,'SortierungTreffer');    
        $this->ProduktObjectArray = $this->ProduktObjectArray;    
```