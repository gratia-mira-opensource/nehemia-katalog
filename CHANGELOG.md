# 1.2.1 ??-??:????
* Verbesserung: Benutzerfreundlichkeit. Eingaben werden im Formularfeld gespeichert.
* Verbesserung: Titelfeld ist nicht mehr zwingend.
* Fehler: Bestand wurde nicht bei allen Abfragen berücksichtigt.
* Fehler: Wenn eine Aktion zwar eingesetzt, aber nicht publiziert ist.
* Fehler: SQL-Fehler, wenn das Stichwort in den Tags gefunden wird behoben.
# 1.2.0 22.05.2024
* Neue Funktion: Kann auch HTML-Code (z.B. für ein Joomlaartikel) generiert werden.
* Neue Funktion: Mit einem *-Zeichen am Ende des Stichworts wird die genaue Suche aufgehoben und das Wortende eines Treffers ist offen.
* Neue Funktion: Es können auch die neusten Produkte einer speziellen Kategorie gesucht werden.
* Aktualisierung: PHP 8.2 vorbereitet.
* Fehler: Fehler behoben, wenn nur ein Eintrag mit entsprechendem Tag gefunden wurde.
* Fehler: Preis wurde z.T. falsch angegeben.
# 1.1.8 08.08.2023
* Klassenname angepasst, damit keine Überschneidung besteht.
# 1.1.7 15.06.2023
* Neue Funktion: Fehler in der Darstellung behoben.
* Neue Funktion: Attribute könnten gefiltert werden (nur im Programmcode).
* Verbesserung: Layout Tabitha.
# 1.1.6 31.10.2022
* Neue Funktion: Wenn man des Schlüsselwort "Alle " (mit Leerschlag vor der Kategorie) eingibt werden alle Produkte der Kategorie, inkl. diesen Produkten, die eine andere Hauptkategorie haben, ausgegeben.
# 1.1.5 10.10.2022
* Neue Schriftart - unterstützt Fremdsprachen
# 1.1.4 26.09.2022
* Neue Funktion: Katalog der neusten Produkte erstellen.
* Verbesserung: Update DOMPDF auf 2.0.1.
# 1.1.3 18.07.2022
* Fehler: Preisreduktionen wurden nicht angezeigt.
* Verbesserung: Rechtschreibung.
# 1.1.2 16.07.2022
* Verbesserung: Produktbeschreibungen können gekürzt werden (Detail-Template).
* Verbesserung: Standarttext zum Katalog wurde angepasst.
* Verbesserung: Leerzeilen werden reduziert.
* Verbesserung: Die Attribute können horizontal angezeigt werden.
# 1.1.1 08.07.2022
* Verbesserung: Rechtsangaben und minimale Anzahl der Produkte können im Backend eingestellt werden.
* Verbesserung: Tabitha-Layout.
# 1.1.0 07.07.2022
* Verbesserung: Man kann mehr Layouts auswählen.
* Verbesserung: Man kann Umschlagbilder überschreiben.
* Verbesserung: Anleitung verbessert.
# 1.0.0 04.06.2022
Erste produktive Version
