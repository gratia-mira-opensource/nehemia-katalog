<?php

// Kein direkter Aufruf der PHP-Datei
defined('_JEXEC') or die('Restricted Access');

// Klassen einbinden
use Joomla\CMS\Factory;

class nk_Tools
{

	// @todo: Ev. Hilfefunktionen und Funktionen die einen Wert zurückgeben trennen
	public static function ersetzeUnicodezeichen($Wert)
	{
		$Suchen   = array('⨯');
		$Ersetzen = array('x');

		return str_replace($Suchen, $Ersetzen, $Wert);
	}

	public static function kuerzenText(string $Text, int $Zeichen = 550)
	{

		if (strlen(strip_tags($Text)) > $Zeichen)
		{

			$HTMLZeichen = strlen($Text) - strlen(strip_tags($Text));
			$Text        = substr($Text, 0, $Zeichen + $HTMLZeichen) . "...";

			// Endpunkt finden
			$StringEndeBR = strrchr($Text, '<br>');
			$StringEndeP  = strrchr($Text, '</p>');
			if (strlen($StringEndeBR) < strlen($StringEndeP))
			{
				$StringEnde = $StringEndeBR;
			}
			else
			{
				$StringEnde = $StringEndeP;
			}
			$Text = str_replace($StringEnde, ' ...<br>', $Text);


			// Offene HTML-Tags schliessen
			// https://gist.github.com/JayWood/348752b568ecd63ae5ce
			preg_match_all('#<([a-z0-9]+)(?: .*)?(?<![/|/ ])>#iU', $Text, $result);
			$openedtags = $result[1];
			preg_match_all('#</([a-z0-9]+)>#iU', $Text, $result);

			$closedtags = $result[1];
			$len_opened = count($openedtags);

			if (count($closedtags) == $len_opened)
			{
				return $Text;
			}
			$openedtags = array_reverse($openedtags);
			for ($i = 0; $i < $len_opened; $i++)
			{
				if (!in_array($openedtags[$i], $closedtags))
				{
					$Text .= '</' . $openedtags[$i] . '>';
				}
				else
				{
					unset($closedtags[array_search($openedtags[$i], $closedtags)]);
				}
			}
		}

		// Abstände abgleichen
		return $Text;
	}

	public static function AutorTitel($Wert)
	{
		$Array = json_decode($Wert);

		if ($Array->field_autor)
		{
			return $Array->field_autor . ': ';
		}
	}


	/**
	 * Ruft die Attribute ab und gibt diese zurück.
	 *
	 * @param   integer  $ProduktID
	 * @param   string   $Format                 Sollen die Attribute nacheinander kommen (Standard) oder pro Zeile nur ein Attribut (vertikal)
	 * @param   array    $AttributeAuschliessen  Attribute, die nicht angezeigt werden sollen.
	 *
	 * @return string HTML-String der Attribute
	 *
	 * @since   unbekannt
	 * @version 1.1.7
	 */
	public static function Attribute(int $ProduktID, string $Format = '', array $AttributeAuschliessen = array())
	{

		$db    = Factory::getContainer()->get('DatabaseDriver');
		$query = $db->getQuery(true);
		$query->select('ad.attribute_name As Name, pad.value As Inhalt')
			->from('#__eshop_attributes AS a')
			->innerJoin('#__eshop_attributedetails AS ad ON (a.id = ad.attribute_id)')
			->innerJoin('#__eshop_productattributes AS pa ON (a.id = pa.attribute_id)')
			->innerJoin('#__eshop_productattributedetails AS pad ON (pa.id = pad.productattribute_id)')
			->where('a.published = 1')
			->where('pa.published = 1')
			->where('pa.product_id = ' . intval($ProduktID))
			->where('ad.language = "de-DE"')
			->where('pad.language = "de-DE"');
		$query->order('a.ordering ASC');
		$db->setQuery($query);

		$Array = $db->loadAssocList();

		if (count($Array) == 1)
		{
			$Semikolon = '';
		}
		else
		{
			$Semikolon = '; ';
		}

		if ($Format == 'vertikal')
		{
			$Attribute = '<p><b>Produktdetails:</b><br><div class="attribute-alle">';
			foreach ($Array as $Attribut)
			{
				if (!in_array($Attribut['Name'], $AttributeAuschliessen))
				{
					$Attribute .= '<div class="attribute-einzel"><span class="attribute-name">' . $Attribut['Name'] . '</span>: <span class="attribute-inhalt">' . $Attribut['Inhalt'] . '</span></div>';
				}
			}
			$Attribute .= '</div></span></p>';
		}
		elseif ($Format == 'html_vertikal')
		{
			$Attribute = '<p><b>Produktdetails:</b><br><p>';
			foreach ($Array as $Attribut)
			{
				if (!in_array($Attribut['Name'], $AttributeAuschliessen))
				{
					$Attribute .= $Attribut['Name'] . ': ' . $Attribut['Inhalt'] . '<br>';
				}
			}
			$Attribute .= '</p>';
		}
		else
		{
			$Attribute = '<p><b>Produktdetails:</b><br><div class="attribute-alle" style="page-break-before: avoid;">';
			foreach ($Array as $Attribut)
			{
				if (!in_array($Attribut['Name'], $AttributeAuschliessen))
				{
					$Attribute .= '<span class="attribute-einzel" style="page-break-before: avoid; white-space: pre;"><span class="attribute-name">' . $Attribut['Name'] . ':</span> <span class="attribute-inhalt">' . $Attribut['Inhalt'] . $Semikolon . '</span></span>&shy;';
				}
			}
			$Attribute .= '</div></span></p>';
		}

		if ($Array)
		{
			return $Attribute;
		}
	}

	public static function Bildbase64($Wert, $Bildquelle = 'Produkt')
	{
		// Bild einfügen
		// https://stackoverflow.com/questions/37952877/adding-an-image-with-dompdf
		if ($Bildquelle == 'Produkt')
		{
			$path = JUri::base() . 'media/com_eshop/products/' . rawurlencode($Wert);
		}
		elseif ($Bildquelle == 'Umschlag')
		{
			$path = JUri::base() . $Wert;
		}
		$type = pathinfo($path, PATHINFO_EXTENSION);
		$data = file_get_contents($path);

		return 'data:image/' . $type . ';base64,' . base64_encode($data);
	}

	/**
	 * @param   string  $Bildname
	 *
	 * @return string
	 * @since 1.2.0
	 */
	public static function Bildlink(string $Bildname)
	{
		return JUri::base() . 'media/com_eshop/products/' . rawurlencode($Bildname);
	}


	public static function Optionen($ProduktID, $Titel = '')
	{

		$db    = Factory::getContainer()->get('DatabaseDriver');
		$query = $db->getQuery(true);
		$query->select('po.price As Preisdifferenz,od.value As Name')
			->from('#__eshop_productoptionvalues As po')
			->innerJoin('#__eshop_optionvaluedetails AS od ON (po.option_value_id = od.optionvalue_id)')
			->where('po.product_id = ' . intval($ProduktID))
			->where('od.language = "de-DE"')
			->where('po.quantity != 0');

		$db->setQuery($query);
		$ObjectArray = $db->loadObjectList();

		if ($ObjectArray)
		{
			$Optionen = '';
			foreach ($ObjectArray as $Option)
			{

				// Warnung, falls der Preis sich ändert
				if ($Option->Preisdifferenz == '0.00000000')
				{
					$Stern = '';
				}
				else
				{
					$Stern = '*';
				}

				$Optionen .= $Option->Name . $Stern . '; ';
			}

			// Reine Optionen ohne Titel
			return '<p>' . $Titel . substr($Optionen, 0, -2) . '</p><br>';
		}

	}


	/**
	 * @param   bool    $NachPreisFragen
	 * @param   string  $Preis
	 * @param   int     $ID
	 *
	 * @return string
	 *
	 * @since unbekannt
	 * @version 1.2.1
	 */
	public static function Preis(bool $NachPreisFragen, string $Preis, int $ID): string
	{
		if (!$NachPreisFragen)
		{
			$db    = Factory::getContainer()->get('DatabaseDriver');
			$query = $db->getQuery(true);
			$query->select('price,date_start,date_end,published')
				->from('#__eshop_productspecials As ps')
				->where('product_id = ' . $ID);

			$db->setQuery($query);
			$Object = $db->loadObject();

			$heute = date("Y-m-d H:i:s");

			if ($Object and $Object->published and $Object->date_start < $heute and $Object->date_end > $heute)
			{
				$Preis = '<span style="color: red"><s>CHF ' . number_format($Preis, 2) . '</s></span> CHF ' . number_format($Object->price, 2) . ' (Aktion bis ' . date_format(date_create($Object->date_end), "d.m.Y") . ')';
			}
			elseif ($Object and $Object->published and ($Object->date_start == '0000-00-00 00:00:00' or $Object->date_start < $heute) and $Object->date_end == '0000-00-00 00:00:00')
			{
				$Preis = '<span style="color: red"><s>CHF ' . number_format($Preis, 2) . '</s></span> CHF ' . number_format($Object->price, 2);
			}
			else
			{
				$Preis = 'CHF ' . number_format($Preis, 2);
			}

			// Prüft den Preis
			if ($Preis == 'CHF 0.00')
			{
				$Preis .= '<br>(Spendenbasis)';
			}
		}
		else
		{
			$Preis = 'Preis auf Nachfrage.';
		}

		return $Preis;
	}

	/**
	 * @param   string  $Wert  Die Beschreibung des Artikels (product_desc).
	 *
	 * @return string
	 * @since unbekannt
	 */
	public static function UntertitelNehemia(string $Wert): string
	{
		preg_match('/<strong><em>(.*)<\/em><\/strong>/Ui', $Wert, $Untertitel);

		// Zweite mögliche Formatierung für Untertitel
		if (empty($Untertitel[0]))
		{
			preg_match('/<em><strong>(.*)<\/strong><\/em>/Ui', $Wert, $Untertitel);
		}

		if ($Untertitel[0])
		{
			return strip_tags($Untertitel[0]) . '<br>';
		}
		else
		{
			return '';
		}
	}

}

?>