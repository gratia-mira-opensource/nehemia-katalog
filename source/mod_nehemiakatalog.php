<?php
/**
 * Module Entry Point
 * 
 * @package    Joomla.Tutorials
 * @subpackage Modules
 * @license    GNU/GPL, see LICENSE.php
 * @link       http://docs.joomla.org/J3.x:Creating_a_simple_module/Developing_a_Basic_Module
 * this is a free software. This version may have been modified pursuant
 * to the GNU General Public License, and as distributed it includes or
 * is derivative of works licensed under the GNU General Public License or
 * other free or open source software licenses.
 */


// No direct access
defined('_JEXEC') or die;

require_once dirname(__FILE__,2) . '/mod_nehemiakatalog/helper.php';
require_once dirname(__FILE__,2) . '/mod_nehemiakatalog/tools.php';
require_once dirname(__FILE__,2) . '/mod_nehemiakatalog/dompdf/autoload.inc.php';

// Konstanten
$oKatalog = new NehemiaKatalog($params);


require JModuleHelper::getLayoutPath('mod_nehemiakatalog');
?>