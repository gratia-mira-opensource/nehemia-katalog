<?php 
// Kein direkter Aufruf der PHP-Datei
defined('_JEXEC') or die('Restricted Access');

// stellt die Überschriften grafisch dar
$this->htmlInhalt .= '<h1>' . $Titel . '</h1>';

for ($i = 0; $i < count($ProduktObjectArray); $i++) {

    // Produkt nicht listen
    if (preg_match('/Katalog:nielisten/', $ProduktObjectArray[$i]->tab5_content) == false) {

        // Einen Rahmen oder nicht?
        if (preg_match('/Katalog:keinRahmen/', $ProduktObjectArray[$i]->tab5_content) == true) {
            $CSSKasse = '';
        } else {
            $CSSKasse = 'class="produktbild"';
        }

        $this->htmlInhalt .= '<div class="box" style="page-break-before: avoid;" ><img src="' .nk_Tools::Bildbase64($ProduktObjectArray[$i]->product_image) . '" width="200px" align="top" '. $CSSKasse .'></div>';
        $this->htmlInhalt .= '<div class="box" style="page-break-before: avoid; page-break-inside:avoid;">
                                    <p style="min-width: 66%;">
                                        <b>' . $ProduktObjectArray[$i]->product_name . '</b><br>' .
                                        nk_Tools::Preis(boolval($ProduktObjectArray[$i]->product_call_for_price),$ProduktObjectArray[$i]->product_price,$ProduktObjectArray[$i]->id) . '<br><br>' .
                                        $ProduktObjectArray[$i]->product_desc . '<br>' .
                                        nk_Tools::Optionen($ProduktObjectArray[$i]->id,'<br><b>Mögliche Optionen:</b> ') .
                                        nk_Tools::Attribute($ProduktObjectArray[$i]->id,'vertikal',array('Waschhinweise')) . '
                                    </p>
                             </div>';
        $this->htmlInhalt .=    '<br><div style="clear: both"><hr style="border: 0px"></div>';
    } 
}
?>