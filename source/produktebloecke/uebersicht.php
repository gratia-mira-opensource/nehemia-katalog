<?php 
// Kein direkter Aufruf der PHP-Datei
defined('_JEXEC') or die('Restricted Access');

// stellt die Überschriften grafisch dar
$this->htmlInhalt .= '<h1>' . $Titel . '</h1>';

$Seite = 0;
for ($i = 0; $i < count($ProduktObjectArray); $i++) {

    // Produkt nicht listen
    if (preg_match('/Katalog:nielisten/', $ProduktObjectArray[$i]->tab5_content) == false) {

        // Vor dem erste Produkt und dem zweiten Div soll es nie die Seite umbrechen
        if ($Seite == 0 or $Seite % 2 != 0) {
            $BoxDivStyle = 'style="page-break-before: avoid;"';
        } else {
            $BoxDivStyle = '';
        }

        // Einen Rahmen oder nicht?
        if (preg_match('/Katalog:keinRahmen/', $ProduktObjectArray[$i]->tab5_content) == true) {
            $CSSKasse = '';
        } else {
            $CSSKasse = 'class="produktbild"';
        }

        $this->htmlInhalt .=    '<div class="box"' . $BoxDivStyle . '><img src="' .nk_Tools::Bildbase64($ProduktObjectArray[$i]->product_image) . '" width="80" align="top" '. $CSSKasse .'></div>
                            <div class="box" style="page-break-before: avoid;"><p style="page-break-inside:avoid; margin: 0; padding: 0; min-width: 200px;">
                                <b>' . nk_Tools::AutorTitel($ProduktObjectArray[$i]->custom_fields) . $ProduktObjectArray[$i]->product_name . '</b><br>'
                                . nk_Tools::Preis(boolval($ProduktObjectArray[$i]->product_call_for_price),$ProduktObjectArray[$i]->product_price,$ProduktObjectArray[$i]->id)
                            . '</p></div>';
        // Zeilenumbruch des Divs nach ungeraden Zahlen
        if ($Seite % 2 != 0) {
            $this->htmlInhalt .=    '<br><div style="clear: both"><hr style="border: 0px"></div>';
        }
        $Seite++;
    } 
}
?>