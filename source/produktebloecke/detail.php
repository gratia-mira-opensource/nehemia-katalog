<?php 
// Kein direkter Aufruf der PHP-Datei
defined('_JEXEC') or die('Restricted Access');

/**
 * @var string $Titel Die Überschrift
 * @var array $ProduktObjectArray Die Informationen des Arrays
 */

// stellt die Überschriften grafisch dar
$this->htmlInhalt .= '<h1>' . $Titel . '</h1>';

for ($i = 0; $i < count($ProduktObjectArray); $i++) {

    // Produkt nicht listen
    if (!str_contains($ProduktObjectArray[$i]->tab5_content, 'Katalog:nielisten')) {

        // Einen Rahmen, oder nicht?
        if (str_contains($ProduktObjectArray[$i]->tab5_content, 'Katalog:keinRahmen')) {
            $CSSKasse = '';
        } else {
            $CSSKasse = 'class="produktbild"';
        }

        $this->htmlInhalt .= '<div class="box" style="page-break-before: avoid;" ><img src="' .nk_Tools::Bildbase64($ProduktObjectArray[$i]->product_image) . '" width="200px" align="top" '. $CSSKasse .'></div>';
        $this->htmlInhalt .= '<div class="box" style="page-break-before: avoid; page-break-inside:avoid;">
                                    <p style="min-width: 66%;">
                                        <b>' . $ProduktObjectArray[$i]->product_name . '</b><br>' .
                                        nk_Tools::Preis(boolval($ProduktObjectArray[$i]->product_call_for_price),$ProduktObjectArray[$i]->product_price,$ProduktObjectArray[$i]->id) . '<br><br>' .
                                        nk_Tools::kuerzenText($ProduktObjectArray[$i]->product_desc) . '<br>' .
                                        nk_Tools::Optionen($ProduktObjectArray[$i]->id,'<br><b>Mögliche Optionen:</b> ') .
                                        nk_Tools::Attribute($ProduktObjectArray[$i]->id) .
	                                    'Bestellnummer: ' . $ProduktObjectArray[$i]->product_sku . '<br>
                                    </p>
                             </div>';
        $this->htmlInhalt .=    '<br><div style="clear: both"><hr style="border: 0"></div>';
    } 
}