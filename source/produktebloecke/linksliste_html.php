<?php 
// Kein direkter Aufruf der PHP-Datei
defined('_JEXEC') or die('Restricted Access');

for ($i = 0; $i < count($ProduktObjectArray); $i++) {

    // Produkt nicht listen
    if (!str_contains($ProduktObjectArray[$i]->tab5_content, 'Katalog:nielisten')) {

		$Artikelname = $ProduktObjectArray[$i]->product_name;
		$Artikellink = JUri::base() . 'index.php/online-store/'. GMF_Allgemein::umbenennenzuAliasname($ProduktObjectArray[$i]->product_alias) . '.html#Main';


		$this->htmlInhalt .= GMF_Layout::erstelleLink($Artikellink,nk_Tools::AutorTitel($ProduktObjectArray[$i]->custom_fields) . $Artikelname,$Artikelname . ' öffnen','_blank') . '<br>';
		$this->htmlInhalt .= "\n";
    } 
}