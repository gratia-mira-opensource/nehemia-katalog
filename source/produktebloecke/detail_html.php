<?php 
// Kein direkter Aufruf der PHP-Datei
defined('_JEXEC') or die('Restricted Access');


for ($i = 0; $i < count($ProduktObjectArray); $i++) {

    // Produkt nicht listen
    if (!str_contains($ProduktObjectArray[$i]->tab5_content, 'Katalog:nielisten')) {

		$Artikelname = $ProduktObjectArray[$i]->product_name;
		$Artikellink = JUri::base() . 'index.php/online-store/'. GMF_Allgemein::umbenennenzuAliasname($ProduktObjectArray[$i]->product_alias) . '.html#Main';
		$Artikelbild = '<img src="' . nk_Tools::Bildlink($ProduktObjectArray[$i]->product_image) . '" alt="' . $Artikelname . '" style="max-width: 250px; width: 250px; margin: 50px;">';

	    $Pfad = dirname(__FILE__,4) . '/media/com_eshop/products/' . $ProduktObjectArray[$i]->product_image;
	    $Hauptfarbe = GMF_Layout::ermittleHauptfarbeRBG($Pfad);

	    $this->htmlInhalt .= '<!-- Start: ' . $Artikelname . ' -->' . "\n";
		$this->htmlInhalt .= '<div class="flex" style="justify-content: flex-start; margin: 0 0 100px 0; background-color: rgba('. $Hauptfarbe .',0.4)">';

        $this->htmlInhalt .= '<div style="width: 300px; object-fit: cover;" >'. GMF_Layout::erstelleLink($Artikellink,$Artikelbild,$Artikelname,'_blank') . '</div>';
        $this->htmlInhalt .= '<div style="margin: 50px;">
                                        <h3>'. GMF_Layout::erstelleLink($Artikellink,'<span style="color: rgb(' . $Hauptfarbe . ');">' . $Artikelname . '</span>',$Artikelname,'_blank') . '</h3><br>' .
                                        $ProduktObjectArray[$i]->product_desc . '<br>' .
                                        nk_Tools::Preis(boolval($ProduktObjectArray[$i]->product_call_for_price),$ProduktObjectArray[$i]->product_price,$ProduktObjectArray[$i]->id) . '<br><br>' .
                                        nk_Tools::Optionen($ProduktObjectArray[$i]->id,'<br><b>Mögliche Optionen:</b> ') .
                                        nk_Tools::Attribute($ProduktObjectArray[$i]->id) . '<br><br>' .
								        GMF_Layout::erstelleLink($Artikellink,'zum Shop',$Artikelname,'_blank') . '<br>' .
                             '</div>
                            </div>'. "\n";
	    $this->htmlInhalt .= '<!-- Ende: ' . $Artikelname . ' -->' . "\n\n";
    } 
}