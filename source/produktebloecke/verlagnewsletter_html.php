

<?php
// Kein direkter Aufruf der PHP-Datei
defined('_JEXEC') or die('Restricted Access');


for ($i = 0; $i < count($ProduktObjectArray); $i++) {

    // Produkt nicht listen
    if (!str_contains($ProduktObjectArray[$i]->tab5_content, 'Katalog:nielisten')) {

		$Artikelname = $ProduktObjectArray[$i]->product_name;
		$Artikellink = JUri::base() . 'index.php/online-store/'. GMF_Allgemein::umbenennenzuAliasname($ProduktObjectArray[$i]->product_alias) . '.html#Main';
		$Artikelbild = '<img src="' . nk_Tools::Bildlink($ProduktObjectArray[$i]->product_image) . '" alt="' . $Artikelname . '" style="max-width: 150px; width: 150px;">';

		// Hauptfarbe festlegen, funktioniert im Moment nur beim JPG.
	    $Pfad = dirname(__FILE__,4) . '/media/com_eshop/products/' . $ProduktObjectArray[$i]->product_image;
		$Hauptfarbe = GMF_Layout::ermittleHauptfarbeRBG($Pfad);

		$Hintergrund = GMF_Layout::konvertiereRGBzuHEX('rgba('. $Hauptfarbe .',0.4)');
		$Titelschrift = GMF_Layout::konvertiereRGBzuHEX('rgb('. $Hauptfarbe .')');

		$this->htmlInhalt .= "\n<!--Start: ". $Artikelname . "-->\n";
		$this->htmlInhalt .=
			'<div class="movableContent">
				<table style="background: '. $Hintergrund . '; border-collapse: collapse;" border="0" width="100%" cellspacing="0" cellpadding="0" align="center">
					<tbody>
					<tr>
						<td style="color: black; width: 1500px;">
							<table style="border-collapse: collapse; width: 600px;" border="0" cellspacing="0" cellpadding="0" align="center">
								<tbody>
								<tr>
									<td style="vertical-align: top; width: 250px;">
										<div class="contentEditableContainer contentImageEditable">
											<div class="contentEditable">
												<div style="padding-top: 20px; text-align: center; margin: 20px;">'. GMF_Layout::erstelleLink($Artikellink,$Artikelbild,$Artikelname,'_blank') . '
												</div>
											</div>
										</div>
									</td>
									<td style="padding-top: 20px; padding-bottom: 5px; text-align: left;" valign="top">
										<p><strong>'. GMF_Layout::erstelleLink($Artikellink,'<span style="font-size: 18pt; color: '. $Titelschrift . '">' . $Artikelname . '</span>',$Artikelname,'_blank') . '</strong></p>
										<div class="contentEditableContainer contentTextEditable">
											<div class="contentEditable" style="line-height: 150%; text-align: left;">
												<br>
												<div class="contentEditableContainer contentTextEditable">
													<div class="contentEditable" style="line-height: 150%; text-align: left;">
														<p>'. $ProduktObjectArray[$i]->product_desc . '<b>' . nk_Tools::Preis(boolval($ProduktObjectArray[$i]->product_call_for_price),$ProduktObjectArray[$i]->product_price,$ProduktObjectArray[$i]->id) . '</b></p>
														<br><br>' .
															nk_Tools::Attribute($ProduktObjectArray[$i]->id,'html_vertikal') . '<br> 
													</div>
												</div>
												<br><br>
												<p style="text-align: justify;">' . GMF_Layout::erstelleLink($Artikellink,'zum Shop',$Artikelname,'_blank') .'</p>
												<br><br>
											</div>
										</div>
									</td>
								</tr>
								</tbody>
							</table>
						</td>
					</tr>
					</tbody>
				</table>
			</div>';
	    $this->htmlInhalt .= "\n<!--Ende: ". $Artikelname . "-->\n";
	}
}
