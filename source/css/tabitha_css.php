<?php 
// Kein direkter Aufruf der PHP-Datei
defined('_JEXEC') or die('Restricted Access');

$this->htmlInhalt .=
        '* {
            font-family: DejaVu Sans, sans-serif;
        }
        footer {
            position: absolute;
            bottom: 20px;
            width: 100%;
            text-align: center;
        }
        h1 {
            color: #a52021;
        }
        p {
            margin-top: 0px;
            margin-bottom: 2px;
            padding: 0px
        }
        .box {
            float: left;
            max-width: 66%;
            margin-right: 2%;
            box-sizing: border-box;
        }

        .box:last-child {
            margin-right: 0;
        }

        .produktbild {
            border-color: #a52021;
            border-width: 1px;
            border-style: outset;
            border-radius: 2px;
        }
        .katalognamen {
            color: #a52021;
            font-size: 60px;
            height: 150px;
            font-weight: bold;
            width: 100%;
            position: absolute;
            text-align: center;
            top: 257px;
        }
        .unterueberschrift {
            font-size: 36px;
        }';
?>