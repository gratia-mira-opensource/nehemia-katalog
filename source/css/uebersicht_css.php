<?php 
// Kein direkter Aufruf der PHP-Datei
defined('_JEXEC') or die('Restricted Access');

$this->htmlInhalt .=
        '* {
            font-family: DejaVu Sans, sans-serif;
        }
        footer {
            position: absolute;
            bottom: 20px;
            width: 100%;
            text-align: center;
        }

        .box {
            float: left;
            max-width: 200px;
            margin-right: 2%;
            box-sizing: border-box;
        }

        .box:last-child {
            margin-right: 0;
        }

        .produktbild {
            border-color: #516da1;
            border-width: 1px;
            border-style: outset;
            border-radius: 2px;
        }
        .katalognamen {
            color: #516da1;
            font-size: 60px;
            height: 150px;
            font-weight: bold;
            width: 100%;
            position: absolute;
            text-align: center;
            top: 257px;
        }
        .unterueberschrift {
            font-size: 36px;
        }';
?>