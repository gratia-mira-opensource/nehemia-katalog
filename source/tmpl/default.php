<?php // Kein direkter Aufruf der PHP-Datei
defined('_JEXEC') or die('Restricted Access'); ?>
    <p style="text-align: left;"><a
                href="https://gitlab.com/gratia-mira-opensource/nehemia-katalog/-/blob/main/README.md"
                title="Zur Dokumentation" target="_blank" rel="noopener">Hilfe</a></p>
    <form action="#Code" method='get'>
        <input type="hidden" name='NehemiaKatolog' value='erstellen'/>
        <input type="text" name='TitelZeile1' value='<?php echo $_GET['TitelZeile1'] ?? ''; ?>' placeholder="Titelzeile" maxlength="18"/>
        <input type="text" name='TitelZeile2' value='<?php echo $_GET['TitelZeile2'] ?? ''; ?>' placeholder="Titelzeile 2" maxlength="32"/>
        <input type="text" name='Stichwort' value='<?php echo $_GET['Stichwort'] ?? ''; ?>' placeholder="Kategorien (4,5) oder Stichwort"/>
		<?php echo $oKatalog->AuswahlLayout; ?>
        <button class="button" title="Katalog erstellen">
            erstellen
        </button>
    </form>
    <!-- Die Kategorien ausgeben -->
    <details>
        <summary>Kategorien anzeigen (hier klicken)</summary>
        <p><?php echo $oKatalog->Kategorienliste; ?></p>
    </details>
    <!--HTML-Ausgabe, wenn erstellt-->
<?php if (isset($oKatalog->HTMLCode))
{
	echo '<h2 id="Code">Code:</h2><hr>';
	echo '<pre>' . htmlentities($oKatalog->HTMLCode) . '</pre>';
	echo '<h2>Vorschau:</h2><hr>';
	echo $oKatalog->HTMLCode;
}