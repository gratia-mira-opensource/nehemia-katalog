<?php

// reference the Dompdf namespace
use Dompdf\Dompdf;
use Joomla\CMS\Factory;

// Kein direkter Aufruf der PHP-Datei
defined('_JEXEC') or die('Restricted Access');


class NehemiaKatalog
{
	// Nach dem Prinzip von:
	// https://www.php-einfach.de/experte/php-codebeispiele/pdf-per-php-erstellen-pdf-rechnung/

	/**
	 * Wird mit dem HTML-Bereich gefüllt. Entweder für den Katalog oder für die direkte HTML-Ausgabe.
	 * @var string
	 * @since 1.0.0
	 */
	public string $htmlInhalt = '';
	/**
	 * Das Stichwort, welches vom Benutzer eingegeben wird.
	 * @var string
	 * @since 1.0.0
	 */
	public string $Stichwort = '';
	/**
	 * Das Layout, das verwendet werden soll.
	 * @var string
	 * @since 1.1.0
	 */
	public string $Layout = '';
	/**
	 * Enthält die Kategorien, welche bei Katalog ausgegeben werden.
	 * @var array
	 * @since 1.0.0
	 */
	public array $KatalogKategorien = array();
	/**
	 * @var boolean
	 * @since 1.1.6
	 */
	private bool $NeueProdukte = false;
	/**
	 * Wie viele neue Produkte angezeigt werden sollen.
	 * @var int
	 * @since 1.1.6
	 *
	 */
	private int $AnzahlNeueProdukte = 0;
	/**
	 * Die Resultate der Datenbankabfrage
	 *
	 * @var array
	 * @since 1.0.0
	 */
	private array $ProduktObjectArray = array();
	/**
	 * @var string
	 * @since 1.2.0
	 */
	public string $HTMLCode = '';
	/**
	 * Die Kategorienliste, welche dem Anwender hilft, die richtigen Kategorien anzuwählen
	 * @var string
	 * @since 1.0.0
	 */
	public string $Kategorienliste = '';
	/**
	 * Gibt das Auswahlfeld für das Layout zurück
	 *
	 * @var string
	 * @since 1.0.0
	 */
	public string $AuswahlLayout = '';
	/**
	 * Bereitet ein Array vor, welches alle Kategorien abspeichert zum späteren Gebrauch.
	 *
	 * @var array
	 * @since 1.0.0
	 */
	private array $KategorieArray = array();
	/**
	 * Die aktuelle Sprache
	 *
	 * @var string
	 * @since 1.2.0
	 *
	 * @todo  Sprache im Backend definieren
	 */
	private string $Sprache = 'de-DE';

	function __construct(&$params)
	{

		// Liest die Parameter aus, welche nur zum Steuern des Moduls notwendig sind.
		$get             = Factory::getApplication();
		$Eingabe         = $get->input;
		$this->Stichwort = trim($Eingabe->get('Stichwort', '', 'String'));
		$this->Layout    = trim($Eingabe->get('Layout', 'linksliste_html', 'String'));
		$TitelZeile1     = trim($Eingabe->get('TitelZeile1', '', 'String'));
		$TitelZeile2     = trim($Eingabe->get('TitelZeile2', '', 'String'));

		$this->vorbereitenBenutzerEingabeFormular();

		// Sichtwort als Titelzeile übernehmen
		if (!$this->Stichwort)
		{
			$this->Stichwort = $TitelZeile1;
		}

		if ($this->Stichwort)
		{
			// Sind es Kategorien oder ein Neuheiten-Katalog?
			if (preg_match('/^A?l?l?e?[, \d]+$/', $this->Stichwort))
			{
				$this->KatalogKategorien = explode(',', str_replace('Alle ', '', $this->Stichwort));
			}
			// Wenn das Schlüsselwort »Neu« verwendet wird, diesen Code ausführen.
			elseif (preg_match('/^neu \d+/i', $this->Stichwort))
			{
				$this->NeueProdukte       = true;
				$this->AnzahlNeueProdukte = intval(trim(preg_replace('/^neu (\d+).*$/i', '$1', $this->Stichwort)));
				// Wenn ein Stichwort vorhanden ist, das Stichwort korrekt setzen.
				if (preg_match('/^neu \d+ ([a-z].*)$/i', $this->Stichwort))
				{
					$this->Stichwort = trim(preg_replace('/^neu \d+ (.*)$/i', '$1', $this->Stichwort));
					// Wenn Kategorien festgelegt wurden, diese verwenden.
					if (preg_match('/^A?l?l?e?[, \d]+$/', $this->Stichwort))
					{
						$this->KatalogKategorien = explode(',', str_replace('Alle ', '', $this->Stichwort));
					}
				}
				else
				{
					$this->Stichwort = 'Neuste';
				}
			}

			// Start html und head Bereich
			if (str_contains($this->Layout, 'html'))
			{
				$this->htmlInhalt = $TitelZeile1 ? '<b>Weitere Produkte zum Thema »' . $TitelZeile1 . "«</b><br>\n" : "<b>Übersicht</b><br>\n";
				$this->htmlInhalt .= $TitelZeile2 ? '<i>' . $TitelZeile2 . "</i>\n" : '';
			}
			else
			{
				$this->htmlInhalt = '<html lang="de"><head><title>' . $TitelZeile1 . '</title></head>';
				$this->erstelleCSS();

				// Ende head, start body Bereich
				$this->htmlInhalt .= '</head><body>';
				// Erste zwei Seiten
				$this->erstelleTitelblatt($TitelZeile1, $TitelZeile2);
			}

			if ($this->KatalogKategorien)
			{

				// Gewählte Kategorien ausgeben
				foreach ($this->KatalogKategorien as $Kategorie)
				{
					// ruft die gewünschten Daten ab
					$this->abfrageDaten($params, $Kategorie);
					// erstellt das HTML Layout
					$this->erstelleEinzelAusgabeLayoutEinheit($this->ProduktObjectArray, $Kategorie);
				}
			}
			elseif ($this->Stichwort)
			{
				// ruft die gewünschten Daten ab
				$this->abfrageDaten($params);
				// erstellt das HTML Layout
				$this->erstelleEinzelAusgabeLayoutEinheit($this->ProduktObjectArray, $this->Stichwort);
			}
			else
			{
				// Anwendung abbrechen
				exit('Bitte Anfrage klar formulieren!');
			}

			if (str_contains($this->Layout, 'html'))
			{
				$Suchen   = array(
					'/<\/?br ?\/?>\r?\n?<p>/',
					'/<\/p>\r?\n?<\/?br ?\/?>/',
					'/<\/?br ?\/?>\r?\n?<\/?br ?\/?>/',
					'/<p>(\&nbsp\;|\s+| )<\/p>/',
				);
				$Ersetzen = array('<p>', '</p>', '<p></p>', '');
			}
			else
			{
				// Letzte zwei Seiten
				$this->erstelleAbschluss($params);
				// Ende HTML
				$this->htmlInhalt .= '</body></html>';
				// Leerzeilen reduzieren, Links egalisieren
				$Suchen   = array(
					'/<\/?br ?\/?>\r?\n?<p>/',
					'/<\/p>\r?\n?<\/?br ?\/?>/',
					'/<\/?br ?\/?>\r?\n?<\/?br ?\/?>/',
					'/<p>(\&nbsp\;|\s+| )<\/p>/',
					'/<a.*>(.*)<\/a>/isU');
				$Ersetzen = array('<p>', '</p>', '<p></p>', '', '$1');
			}

			// Zweimal, dass sicher jedes Vorkommen ersetzt wird.
			$this->htmlInhalt = preg_replace($Suchen, $Ersetzen, $this->htmlInhalt);
			$this->htmlInhalt = preg_replace($Suchen, $Ersetzen, $this->htmlInhalt);
			// echo $this->htmlInhalt;
			// exit;

			if (str_contains($this->Layout, 'html'))
			{
				$this->HTMLCode = $this->htmlInhalt;
			}
			else
			{
				// PDF ausgeben; Namen generieren
				$pdfNamen = preg_replace(array('/ä/i', '/ö/i', '/ü/i', '/[^A-Za-z0-9 -]+/'), array('ae', 'oe', 'ue', ''), 'Katalog - ' . $TitelZeile1 . ' ' . $TitelZeile2 . ' - Nehemia') . '.pdf';
				$this->erstellePDFAusgabe(nk_Tools::ersetzeUnicodezeichen($this->htmlInhalt), $pdfNamen);
			}
		}

	}

	private function vorbereitenBenutzerEingabeFormular()
	{

		// Kategorien auslesen
		$db    = Factory::getContainer()->get('DatabaseDriver');
		$query = $db->getQuery(true);

		$query->select('category_name, category_id')
			->from('#__eshop_categorydetails AS a')
			->where('language = "' . $this->Sprache . '"')
			->order('category_name ASC');
		$db->setQuery($query);

		$KategorieArray = $db->loadObjectList();

		$i = 0;
		foreach ($KategorieArray as $Zeile)
		{

			$this->Kategorienliste .= $Zeile->category_name . ': <b>' . $Zeile->category_id . '</b><br>';

			// Array vorbereiten
			$this->KategorieArray[$i]['id']   = $Zeile->category_id;
			$this->KategorieArray[$i]['name'] = $Zeile->category_name;
			$i++;
		}

		// Layouts auslesen und Auswahl vorbereiten
		$DateienLayoutOrdnerArray = scandir(dirname(__FILE__, 1) . '/produktebloecke', 1);

		$Layout = '<label for="Layout">Ein Layout wählen:</label>
                        <select name="Layout" id="Layout" required>';
		foreach ($DateienLayoutOrdnerArray as $Datei)
		{
			if (preg_match('/\.php$/', $Datei))
			{
				$LayoutName = preg_replace('/\.php$/', '', $Datei);
				$Gewaehlt   = ($LayoutName == $this->Layout) ? 'selected' : '';
				$Layout     .= '<option value="' . $LayoutName . '" ' . $Gewaehlt . '>' . ucfirst($LayoutName) . '</option>';
			}
		}

		$Layout              .= '</select>';
		$this->AuswahlLayout = $Layout;

	}

	/**
	 * Gibt ein ObjektArray aller Treffer zurück
	 *
	 * @param $params    Joomla\Registry\Registry Joomla Administrator Moduleinstellungen
	 * @param $Kategorie integer Die aktuelle Kategorie
	 *
	 * @since   1.0.0
	 * @version 1.2.1
	 */
	private function abfrageDaten(Joomla\Registry\Registry $params, int $Kategorie = 0): void
	{

		// Datenbankabfrage
		$db = Factory::getContainer()->get('DatabaseDriver');

		// Inner Join bestimmen
		// mit Nebenkategorien oder nur Hauptkategorie
		if (preg_match('/^Alle[, \d]+$/', $this->Stichwort))
		{
			$InnerJoinKategorie = '#__eshop_productcategories AS c ON a.id = c.product_id AND a.published = 1 AND b.language = "' . $this->Sprache . '"';
		}
		else
		{
			$InnerJoinKategorie = '#__eshop_productcategories AS c ON a.id = c.product_id AND main_category = 1 AND a.published = 1 AND b.language = "' . $this->Sprache . '"';
		}

		// Filter bestimmen
		if ($this->KatalogKategorien)
		{
			$qAbfrage = 'c.category_id IN (' . $Kategorie . ')';

		}
		elseif ($this->Stichwort)
		{
			if ($this->Stichwort == 'Neuste')
			{
				// Alle Produkte finden
				$qAbfrage = 'b.product_name REGEXP "."';
			}
			else
			{
				if (preg_match('/\*$/', $this->Stichwort))
				{
					$this->Stichwort = preg_replace('/\*$/', '', $this->Stichwort);
					$REGEXPWortende  = '';
				}
				else
				{
					$REGEXPWortende = '[[:>:]]';
				}

				$qAbfrage = '(b.product_name REGEXP "[[:<:]]' . $this->Stichwort . $REGEXPWortende . '" OR b.product_desc REGEXP "[[:<:]]' . $this->Stichwort . $REGEXPWortende . '"';

				// Vorbereitungsabfrage
				$query = $db->getQuery(true);
				$query->select('a.product_id')
					->from('#__eshop_producttags AS a')
					->innerJoin('#__eshop_tags AS b ON a.tag_id = b.id')
					->where('tag_name like' . $db->quote('%' . $this->Stichwort . '%'));

				$db->setQuery($query);
				$ProduktIDArray = $db->loadColumn();

				// Suchstring ergänzen (int, weil $db->quote hier nicht funktioniert)
				if (isset($ProduktIDArray) and count($ProduktIDArray) > 1)
				{
					$qAbfrage .= ' OR b.product_id IN (' . implode(",", $ProduktIDArray) . ')) ';
				}
				elseif (isset($ProduktIDArray) and count($ProduktIDArray) == 1)
				{
					$qAbfrage .= ' OR b.product_id LIKE "' . implode(",", $ProduktIDArray) . '") ';
				}
				else
				{
					$qAbfrage .= ') ';
				}
			}
		}
		else
		{
			// Beende die Anweisung!
			exit;
		}

		// Für alle Abfragen geltende Filter
		$qAbfrage .= 'AND a.product_quantity >= ' . $params->get('MinBestand') . ' '; // Nur gewisse Lagermenge
		/** @todo Verbotene Begriffe übers Backend einstellen */
		$qAbfrage .= 'AND b.product_name NOT LIKE "%E-Book%" AND b.product_name NOT LIKE "%eBook%"'; // Keine eBooks

		// Eigentliche Abfrage
		$query = $db->getQuery(true);

		if ($this->NeueProdukte)
		{
			$query->select('a.*, b.product_name, b.product_alias, b.product_desc, b.product_short_desc, b.product_page_title, b.product_page_heading, b.product_alt_image, b.meta_key, b.meta_desc, b.tab1_title, b.tab1_content, b.tab2_title, b.tab2_content, b.tab3_title, b.tab3_content, b.tab4_title, b.tab4_content, b.tab5_title, b.tab5_content,c.category_id')
				->from('#__eshop_products AS a')
				->innerJoin('#__eshop_productdetails AS b ON a.id = b.product_id')
				->innerJoin($InnerJoinKategorie)
				// Einschränkung Filter
				->where($qAbfrage)
				->order('b.product_id DESC');

			$db->setQuery($query, 0, $this->AnzahlNeueProdukte);
		}
		else
		{
			$query->select('a.*, b.product_name, b.product_alias, b.product_desc, b.product_short_desc, b.product_page_title, b.product_page_heading, b.product_alt_image, b.meta_key, b.meta_desc, b.tab1_title, b.tab1_content, b.tab2_title, b.tab2_content, b.tab3_title, b.tab3_content, b.tab4_title, b.tab4_content, b.tab5_title, b.tab5_content,c.category_id')
				->from('#__eshop_products AS a')
				->innerJoin('#__eshop_productdetails AS b ON a.id = b.product_id')
				->innerJoin($InnerJoinKategorie)
				// Einschränkung Filter
				->where($qAbfrage)
				// Sortierung nach ABC ohne Anführungszeichen
				->order('REGEXP_REPLACE(TRIM(b.product_name),"[\"\']","") ASC');

			$db->setQuery($query);
		}

		$this->ProduktObjectArray = $db->loadObjectList();
		// print_r($db->loadObjectList());
		// exit;
	}

	private function erstelleCSS()
	{
		$this->htmlInhalt .= '<style>';
		include('css/' . $this->Layout . '_css.php');
		$this->htmlInhalt .= '</style>';
	}

	private function erstelleTitelblatt($TitelZeile1, $TitelZeile2)
	{

		if (!empty($TitelZeile2))
		{
			$Katalognamen = $TitelZeile1 . '<br><span class="unterueberschrift">' . $TitelZeile2 . '</span>';
		}
		else
		{
			$Katalognamen = '<div style="margin-top: 5%;">' . $TitelZeile1 . '</div>';
		}

		if (file_exists(dirname(__FILE__, 3) . '/images/nehemia-katalog/' . $this->Layout . '_umschlag1.jpg'))
		{
			$this->htmlInhalt .= '<img src="' . nk_Tools::Bildbase64('images/nehemia-katalog/' . $this->Layout . '_umschlag1.jpg', 'Umschlag') . '" width="100%" align="top"><div style="clear: both"><div class="katalognamen">' . $Katalognamen . '</div></div>';
		}
		else
		{
			$this->htmlInhalt .= '<img src="' . nk_Tools::Bildbase64('modules/mod_nehemiakatalog/umschlagsdateien/standart_umschlag1.jpg', 'Umschlag') . '" width="100%" align="top"><div style="clear: both"><div class="katalognamen">' . $Katalognamen . '</div></div>';
		}
		if (file_exists(dirname(__FILE__, 3) . '/images/nehemia-katalog/' . $this->Layout . '_umschlag2.jpg'))
		{
			$this->htmlInhalt .= '<img src="' . nk_Tools::Bildbase64('images/nehemia-katalog/' . $this->Layout . '_umschlag2.jpg', 'Umschlag') . '" width="100%" align="top"><div style="clear: both"></div>';
		}
		else
		{
			$this->htmlInhalt .= '<img src="' . nk_Tools::Bildbase64('modules/mod_nehemiakatalog/umschlagsdateien/standart_umschlag2.jpg', 'Umschlag') . '" width="100%" align="top"><div style="clear: both"></div>';
		}
	}

	private function erstelleEinzelAusgabeLayoutEinheit($ProduktObjectArray, $Titelinfo)
	{
		if ($ProduktObjectArray)
		{
			// ruft den richtigen Titel ab
			if ($this->KatalogKategorien)
			{
				$ArrayKey = array_search($Titelinfo, array_column($this->KategorieArray, 'id'));
				$Titel    = $this->KategorieArray[$ArrayKey]['name'];
			}
			else
			{
				if ($this->Stichwort == 'Neuste')
				{
					$Titel = 'Neuste Produkte';
				}
				else
				{
					$Titel = 'Produkte zum Stichwort «' . $Titelinfo . '»';
				}
			}
			// HTML Aufbau
			include('produktebloecke/' . $this->Layout . '.php');
		}

		$this->htmlInhalt .= '<div style="clear: both"></div>';
	}

	/**
	 * Abschlussseite des Katalogs erstellen.
	 *
	 * @param $params
	 *
	 * @since   1.0.0
	 *
	 */
	private function erstelleAbschluss($params): void
	{

		$Copyright = '<footer> Copyright &copy; ' . $params->get('Rechtinhaber') . ' ' . date("m.Y") . '; ' . $params->get('HinweisSortiment') . ' </footer><br>';


		// Zweitletzte Seite
		if (file_exists(dirname(__FILE__, 3) . '/images/nehemia-katalog/' . $this->Layout . '_umschlag3.jpg'))
		{
			$this->htmlInhalt .= '<img src="' . nk_Tools::Bildbase64('images/nehemia-katalog/' . $this->Layout . '_umschlag3.jpg', 'Umschlag') . '" width="100%" align="top"><div style="clear: both">' . $Copyright . '</div>';
		}
		else
		{
			$this->htmlInhalt .= '<img src="' . nk_Tools::Bildbase64('modules/mod_nehemiakatalog/umschlagsdateien/standart_umschlag3.jpg', 'Umschlag') . '" width="100%" align="top"><div style="clear: both">' . $Copyright . '</div>';
		}

		// Letzte Seite
		if (file_exists(dirname(__FILE__, 3) . '/images/nehemia-katalog/' . $this->Layout . '_umschlag4.jpg'))
		{
			$this->htmlInhalt .= '<img src="' . nk_Tools::Bildbase64('images/nehemia-katalog/' . $this->Layout . '_umschlag4.jpg', 'Umschlag') . '" width="100%" align="top"><div style="clear: both"></div>';
		}
		else
		{
			$this->htmlInhalt .= '<img src="' . nk_Tools::Bildbase64('modules/mod_nehemiakatalog/umschlagsdateien/standart_umschlag4.jpg', 'Umschlag') . '" width="100%" align="top"><div style="clear: both"></div>';
		}

	}

	private function erstellePDFAusgabe($htmlInhalt, $pdfNamen)
	{

		// Verhindern, dass PDFs im Cache gespeichert werden.
		header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
		header("Expires: Sat, 01 Jan 2000 00:00:00 GMT");

		// https://github.com/dompdf/dompdf
		$dompdf = new Dompdf();

		$options = $dompdf->getOptions();
		$options->setDefaultFont('DejaVu Sans');
		$options->setisPhpEnabled(true);
		$dompdf->setOptions($options);

		// instantiate and use the dompdf class
		$dompdf->loadHtml($htmlInhalt);

		// (Optional) Setup the paper size and orientation
		$dompdf->setPaper('A4', 'portrait');

		// Render the HTML as PDF
		$dompdf->render();

		// https://stackoverflow.com/questions/35491339/make-dompdf-skip-first-page-when-numbering-pages-with-page-num
		$dompdf->getCanvas()->page_script('
            if ($PAGE_NUM > 2 and $PAGE_NUM < ($PAGE_COUNT-1)) {
                $current_page = $PAGE_NUM;
                $pdf->text(520, 790, "Seite $current_page", null, 10, array(0,0,0));
            }
            ');

		// Output the generated PDF to Browser
		$dompdf->stream($pdfNamen);
		exit();
	}
}